#!/usr/bin/python
def is_palindrome(s):
    # reverse the string
    rev_s = reversed(s)

    # check if the string is equal to its reverse
    return list(s) == list(rev_s)